package com.equipe03.gti350.fitnets;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chou Trang on 7/15/2015.
 */
public class DaysDataSource {
    // Database fields
    private SQLiteDatabase database;
    private NewSQLiteHelper dbHelper;
    private String[] allColumns = { NewSQLiteHelper.COLUMN_ID,
            NewSQLiteHelper.COLUMN_DAY, NewSQLiteHelper.COLUMN_WORKOUT};

    public DaysDataSource(Context context) {
        dbHelper = new NewSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Day createDay(String dayName, Workout workout) {
        ContentValues values = new ContentValues();
        values.put(NewSQLiteHelper.COLUMN_DAY, dayName);
        values.put(NewSQLiteHelper.COLUMN_WORKOUT, workout.getWorkout());

        long insertId = database.insert(NewSQLiteHelper.TABLE_DAYS, null,
                values);
        Cursor cursor = database.query(NewSQLiteHelper.TABLE_DAYS,
                allColumns, NewSQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Day day = cursorToDay(cursor);
        cursor.close();
        return day;
    }

    public void deleteDay(Day day) {
        long id = day.getId();
        System.out.println("Workout deleted with id: " + id);
        database.delete(NewSQLiteHelper.TABLE_DAYS, NewSQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public void deleteAllDays(){
        database.delete(NewSQLiteHelper.TABLE_DAYS, null,null);
    }

    public List<Day> getDays() {
        List<Day> days = new ArrayList<Day>();

        Cursor cursor = database.query(NewSQLiteHelper.TABLE_DAYS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Day day = cursorToDay(cursor);
            days.add(day);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return days;
    }

    private Day cursorToDay(Cursor cursor) {
        Day day = new Day();
        day.setId(cursor.getLong(0));
        day.setDay(cursor.getString(1));
        day.setWorkout(cursor.getString(2));
        return day;
    }
}
