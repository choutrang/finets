package com.equipe03.gti350.fitnets;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class AssignRepActivity extends ActionBarActivity {

    private ExercicesDataSource datasource;
    private WorkoutsDataSource workoutsDataSource;
    private WorkoutExercicesDataSource workoutExerciceDatasource;
    private DaysDataSource daysDataSource;

    private ListView lv;
    private List<Exercice> exercicesSelected;
    private List<String> exercicesSelectedName;
    private String workoutname;
    private String day;
    private List<String> listReps = new ArrayList<String>();
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_rep);

        loadDatabase();

        intent = getIntent();
        Bundle bundle = intent.getExtras();

        exercicesSelectedName = bundle.getStringArrayList("exercices");
        workoutname = bundle.getString("workout");
        day = bundle.getString("daySelected");
        List<Exercice> exercices = datasource.getAllExercices();
        exercicesSelected = new ArrayList<Exercice>();

        for (int i = 0; i< exercices.size(); i++) {
            for(int z = 0; z<exercicesSelectedName.size(); z++) {
                if (exercices.get(i).getExercice().equals(exercicesSelectedName.get(z))) {
                    exercicesSelected.add(exercices.get(i));
                }
            }
        }

        ArrayAdapter<Exercice> adapter = new ExerciceRepListAdapter(this, exercicesSelected);
        lv = (ListView) findViewById(R.id.listViewChooseExercice);
        lv.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_assign_rep, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.buttonSaveWorkout:
                //createWorkout();
                saveWorkout();
                break;
            default:
                break;

        }
    }

    public void saveWorkout(){
        listReps.clear();
        for (int i =0; i<lv.getAdapter().getCount();i++) {
            View view = lv.getChildAt(i);
            EditText edSet1 = (EditText) view.findViewById(R.id.editText);
            EditText edSet2 = (EditText) view.findViewById(R.id.editText2);
            EditText edSet3 = (EditText) view.findViewById(R.id.editText3);
            EditText edSet4 = (EditText) view.findViewById(R.id.editText4);
            EditText edSet5 = (EditText) view.findViewById(R.id.editText5);
            TextView exerciceName = (TextView) view.findViewById(R.id.Itemname);

            String reps = edSet1.getText() + " " + edSet2.getText() + " " + edSet3.getText() + " " + edSet4.getText() + " " + edSet5.getText();
            if(reps.trim().length()>0) {
                listReps.add(reps);
                //Toast.makeText(getApplicationContext(), exerciceName.getText() + " Reps: " + reps,
                //       Toast.LENGTH_SHORT).show();
            }


        }
        createWorkout();

    }

    private void createWorkout(){

        String message = "";
        boolean isRepsPresent = false;
        int position=0;

        if(!listReps.isEmpty() && listReps.size() == exercicesSelected.size()){

            isRepsPresent = true;

        }else{
            message += "Please specify at least one set for each exercice";

        }


        if(isRepsPresent == true){
            workoutsDataSource.createExercice(workoutname);
            List<Workout> listeWorkouts = workoutsDataSource.getAllWorkouts();

            for(Exercice exercice: exercicesSelected) {
                workoutExerciceDatasource.createWorkoutExercice((Workout)listeWorkouts.get(listeWorkouts.size()-1),exercice,listReps.get(position),60);
                position++;
            }

            daysDataSource.createDay(day,(Workout)listeWorkouts.get(listeWorkouts.size()-1));
            Toast.makeText(getApplicationContext(), "WORKOUT SAVED",
                    Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        }else{
            Toast.makeText(getApplicationContext(), message,
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void loadDatabase(){
        datasource = new ExercicesDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutsDataSource = new WorkoutsDataSource(this);
        try {
            workoutsDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutExerciceDatasource = new WorkoutExercicesDataSource(this);
        try {
            workoutExerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        daysDataSource = new DaysDataSource(this);
        try {
            daysDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
