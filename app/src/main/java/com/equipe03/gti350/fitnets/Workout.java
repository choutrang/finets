package com.equipe03.gti350.fitnets;

/**
 * Created by Chou Trang on 7/15/2015.
 */
public class Workout {
    private long id;
    private String workout;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWorkout() {
        return workout;
    }

    public void setComment(String comment) {
        this.workout = comment;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return workout;
    }
}
