package com.equipe03.gti350.fitnets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Chou Trang on 7/20/2015.
 * http://www.androidinterview.com/android-custom-listview-with-image-and-text-using-arrayadapter/
 * http://hmkcode.com/android-custom-listview-items-row/CustomListAdapter
 */
public class ExerciceRepListAdapter extends ArrayAdapter<Exercice> {
    private final Context context;
    private final List<Exercice> itemsArrayList;

    public ExerciceRepListAdapter(Context context, List<Exercice> itemsArrayList) {

        super(context, R.layout.mylistassignrep, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.mylistassignrep, parent, false);

        // 3. Get the two text view from the rowView
        TextView labelView = (TextView) rowView.findViewById(R.id.Itemname);

        // 4. Set the text for textView

        String title = itemsArrayList.get(position).toString();
        labelView.setText(title);



        return rowView;

    };
}
