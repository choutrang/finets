package com.equipe03.gti350.fitnets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Chou Trang on 7/20/2015.
 * http://www.androidinterview.com/android-custom-listview-with-image-and-text-using-arrayadapter/
 * http://hmkcode.com/android-custom-listview-items-row/CustomListAdapter
 */
public class ExerciceListCheckboxAdapter extends ArrayAdapter<Exercice> {
    private final Context context;
    private final List<Exercice> itemsArrayList;
    public boolean[] itemChecked;

    public ExerciceListCheckboxAdapter(Context context, List<Exercice> itemsArrayList) {

        super(context, R.layout.mylistcheckbox, itemsArrayList);
        itemChecked = new boolean[itemsArrayList.size()];
        this.context = context;
        this.itemsArrayList = itemsArrayList;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        //View rowView = inflater.inflate(R.layout.mylistcheckbox, parent, false);

        // 3. Get the two text view from the rowView
        //TextView labelView = (TextView) rowView.findViewById(R.id.Itemname2);
        //ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        //CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.checkBox);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.mylistcheckbox, parent, false);
            holder = new ViewHolder();

            holder.itemname = (TextView) convertView
                    .findViewById(R.id.Itemname2);
            holder.ck1 = (CheckBox) convertView
                    .findViewById(R.id.checkBox);
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }
        holder.ck1.setChecked(false);

        if (itemChecked[position])
            holder.ck1.setChecked(true);
        else
            holder.ck1.setChecked(false);

        holder.ck1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.ck1.isChecked())
                    itemChecked[position] = true;
                else
                    itemChecked[position] = false;
            }
        });

        // 4. Set the text for textView
        TextView labelView = (TextView) convertView.findViewById(R.id.Itemname2);
        String title = itemsArrayList.get(position).toString();
        labelView.setText(title);

        return convertView;

    };

    private class ViewHolder {
        TextView itemname;
        CheckBox ck1;
    }
}
