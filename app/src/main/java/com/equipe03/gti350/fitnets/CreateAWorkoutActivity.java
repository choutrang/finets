package com.equipe03.gti350.fitnets;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CreateAWorkoutActivity extends ActionBarActivity {
    private ExercicesDataSource datasource;
    private WorkoutsDataSource workoutsDataSource;
    private WorkoutExercicesDataSource workoutExerciceDatasource;
    private ListView lv;
    private List<Exercice> exercicesSelected;
    private List<String> exercicesSelectedName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_aworkout);
        setTitle(R.string.title_activity_main);

        datasource = new ExercicesDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutsDataSource = new WorkoutsDataSource(this);
        try {
            workoutsDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutExerciceDatasource = new WorkoutExercicesDataSource(this);
        try {
            workoutExerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Exercice> values = datasource.getAllExercices();

        ArrayAdapter<Exercice> adapter = new ExerciceListCheckboxAdapter(this, values);
        lv = (ListView) findViewById(R.id.listViewChooseExercice);
        lv.setAdapter(adapter);

        exercicesSelected= new ArrayList<Exercice>();
        exercicesSelectedName = new ArrayList<String>();

        //http://stackoverflow.com/questions/5551042/onitemclicklistener-not-working-in-listview-android
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
                Object exerciceClicked = lv.getAdapter().getItem(position);
                String text = "";

                if(!checkBox.isChecked()) {
                    checkBox.performClick();
                    checkBox.setChecked(true);
                    exercicesSelected.add((Exercice)exerciceClicked);
                    exercicesSelectedName.add(((Exercice) exerciceClicked).getExercice());
                    text = "Exercice added: " + exerciceClicked + " \nPosition: " + id ;
                }else{
                    checkBox.performClick();
                    checkBox.setChecked(false);
                    exercicesSelected.remove((Exercice) exerciceClicked);
                    exercicesSelectedName.remove(((Exercice) exerciceClicked).getExercice());
                    text = "Exercice removed: " + exerciceClicked + " \nPosition: " + id;
                }
                //Toast.makeText(getApplicationContext(), text,
                  //      Toast.LENGTH_SHORT).show();
            }

        });

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapterSpin = ArrayAdapter.createFromResource(this,
                R.array.days_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapterSpin);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_aworkout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.buttonSaveWorkout:
                //createWorkout();
                assignRep();
                break;
            default:
                break;

        }
    }

    private void assignRep(){

        Intent intent = new Intent(this, AssignRepActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        EditText etWorkoutName = (EditText) findViewById(R.id.fworkoutname);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        String day = "";
        String workoutname = "";
        String message = "";
        boolean isNameWorkoutPresent = false;
        boolean isExercicePresent = false;


        //get day
        day = spinner.getSelectedItem().toString();

        if(etWorkoutName.getText().toString().trim().length()>0){
            isNameWorkoutPresent = true;
            workoutname = etWorkoutName.getText().toString();

            //Toast.makeText(getApplicationContext(), workoutname,
            //        Toast.LENGTH_SHORT).show();

        }else{
            message = "Please name your workout";
        }

        if(!exercicesSelected.isEmpty() && exercicesSelected.size()>1){
            isExercicePresent = true;
        }else{
            message += "Please add a minimum of two exercices to your workout";

        }


        if(isNameWorkoutPresent == true && isExercicePresent == true){
            intent.putExtra("workout",workoutname);
            intent.putExtra("daySelected", day);
            intent.putStringArrayListExtra("exercices", (ArrayList<String>)exercicesSelectedName);
            startActivity(intent);
        }

        if(message!="") {
            Toast.makeText(getApplicationContext(), message,
                    Toast.LENGTH_LONG).show();
        }

    }

    /*
    private void createWorkout(){
        EditText etWorkoutName = (EditText) findViewById(R.id.fworkoutname);
        String workoutname = "";
        String message = "";
        boolean isNameWorkoutPresent = false;
        boolean isExercicePresent = false;


        if(etWorkoutName.getText().toString().trim().length()>0){
            isNameWorkoutPresent = true;
            workoutname = etWorkoutName.getText().toString();
            Toast.makeText(getApplicationContext(), workoutname,
                    Toast.LENGTH_SHORT).show();
            //workoutsDataSource.createExercice(workoutname);
        }else{
            message = "ERROR!! Please name your workout";
        }

        if(!exercicesSelected.isEmpty()){
            isExercicePresent = true;
        }else{
            message += "ERROR!! Please add a minimum of one exercice to your workout";
        }


        if(isNameWorkoutPresent == true && isExercicePresent == true){
            workoutsDataSource.createExercice(workoutname);
            List<Workout> listeWorkouts = workoutsDataSource.getAllWorkouts();

            for(Exercice exercice: exercicesSelected) {
                workoutExerciceDatasource.createWorkoutExercice((Workout)listeWorkouts.get(listeWorkouts.size()-1),exercice,"10,15,20,25",60);
            }
        }

    }*/
}
