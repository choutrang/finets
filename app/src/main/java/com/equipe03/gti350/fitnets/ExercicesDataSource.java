package com.equipe03.gti350.fitnets;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chou Trang on 7/15/2015.
 */
public class ExercicesDataSource {
    // Database fields
    private SQLiteDatabase database;
    private NewSQLiteHelper dbHelper;
    private String[] allColumns = { NewSQLiteHelper.COLUMN_ID,
            NewSQLiteHelper.COLUMN_EXERCICE, NewSQLiteHelper.COLUMN_DESCRIPTION,
            NewSQLiteHelper.COLUMN_REP, NewSQLiteHelper.COLUMN_REST_TIME };

    public ExercicesDataSource(Context context) {
        dbHelper = new NewSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Exercice createExercice(String exercice, String description, String rep, String restTime) {
        ContentValues values = new ContentValues();
        values.put(NewSQLiteHelper.COLUMN_EXERCICE, exercice);
        values.put(NewSQLiteHelper.COLUMN_DESCRIPTION, description);
        values.put(NewSQLiteHelper.COLUMN_REP, rep);
        values.put(NewSQLiteHelper.COLUMN_REST_TIME, restTime);

        long insertId = database.insert(NewSQLiteHelper.TABLE_EXERCICES, null,
                values);

        Cursor cursor = database.query(NewSQLiteHelper.TABLE_EXERCICES,
                allColumns, NewSQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);

        cursor.moveToFirst();
        Exercice newExercice = cursorToExercice(cursor);
        cursor.close();
        return newExercice;
    }

    public void deleteExercice(Exercice exercice) {
        long id = exercice.getId();
        System.out.println("Workout deleted with id: " + id);
        database.delete(NewSQLiteHelper.TABLE_EXERCICES, NewSQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public void deleteAllExercices(){
        List<Exercice> exercices = getAllExercices();

        for (Exercice exercice : exercices){
            database.delete(NewSQLiteHelper.TABLE_EXERCICES, NewSQLiteHelper.COLUMN_ID
                    + " = " + exercice.getId(), null);
        }
    }

    public List<Exercice> getAllExercices() {
        List<Exercice> exercices = new ArrayList<Exercice>();

        Cursor cursor = database.query(NewSQLiteHelper.TABLE_EXERCICES,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Exercice exercice = cursorToExercice(cursor);
            exercices.add(exercice);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return exercices;
    }
    private Exercice cursorToExercice(Cursor cursor) {
        Exercice exercice = new Exercice();
        exercice.setId(cursor.getLong(0));
        exercice.setExercice(cursor.getString(1));
        return exercice;
    }
}
