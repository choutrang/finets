package com.equipe03.gti350.fitnets;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.List;


public class CreateExerciceActivity extends ActionBarActivity {

    private WorkoutsDataSource datasource;
    private ExercicesDataSource exerciceDatasource;
    private WorkoutExercicesDataSource workoutExerciceDatasource;
    private DaysDataSource daysDatasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_exercice);
        setTitle("Developper Settings");

        datasource = new WorkoutsDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        exerciceDatasource = new ExercicesDataSource(this);
        try {
            exerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutExerciceDatasource = new WorkoutExercicesDataSource(this);
        try {
            workoutExerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        daysDatasource = new DaysDataSource(this);
        try {
            daysDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_exercice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {

        Workout workout = null;
        Exercice exercice = null;
        Toast toast;
        int duration;
        duration = Toast.LENGTH_LONG;
        CharSequence text;
        String defaultRep = "15,20,30";
        String defaultRest = "60";
        switch (view.getId()) {
            case R.id.buttonDefaultWorkout:
                String[] workouts = new String[] { "Cardio Workout", "Strength Workout", "Hypertrophy Workout", "HIIT Workout", "Core Workout" };
                // save the new comment to the database

                for (int i = 0; i<workouts.length; i++) {
                    workout = datasource.createExercice(workouts[i]);
                }
                text = "Default Exercice Applied";

                toast = Toast.makeText(this, text, duration);
                toast.show();
                break;
            case R.id.buttonDeleteAllWorkout:
                datasource.deleteAllWorkouts();
                break;

            case R.id.buttonDefaultExercice:

                String[] exercices = new String[] { "Jumping Jack", "Push Up", "Sit up", "Bench Press", "Burpee" };
                String[] descriptions = new String[] { getString(R.string.jumpingjack_desc),
                        getString(R.string.pushup_desc), getString(R.string.situp_desc),
                        getString(R.string.benchpress_desc), getString(R.string.burpee_desc) };



                // save the new comment to the database

                for (int i = 0; i<exercices.length; i++) {
                    exercice = exerciceDatasource.createExercice(exercices[i],descriptions[i],defaultRep,defaultRest);
                }
                text = "Default Exercice Applied";

                toast = Toast.makeText(this, text, duration);
                toast.show();
                break;

            case R.id.buttonDeleteAllExercice:
                exerciceDatasource.deleteAllExercices();
                break;

            //Associate default exercice
            case R.id.buttonAssociateWorkoutExercice:
                List<Workout> listeWorkouts = datasource.getAllWorkouts();
                List<Exercice> listExercice = exerciceDatasource.getAllExercices();
                int defaultRests = 60;


                for (int i = 0; i<listeWorkouts.size(); i++) {
                    for(int z = 0; z<listeWorkouts.size(); z++) {
                        workoutExerciceDatasource.createWorkoutExercice(listeWorkouts.get(z), listExercice.get(i), defaultRep, defaultRests);
                    }
                }

                text = "Associated " + listeWorkouts.get(0) + " with " + listExercice.get(0);

                text = (workoutExerciceDatasource.getAllWorkoutExercices(listeWorkouts.get(0))).toString();


                toast = Toast.makeText(this, text, duration);
                toast.show();
                break;

            case R.id.buttonCreateDay:
                //daysDatasource.deleteAllDays();
                String days[] = new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
                List<Workout> listeWorkouts2 = datasource.getAllWorkouts();

                //for (int i = 0; i<days.length; i++) {
                    daysDatasource.createDay(days[0], listeWorkouts2.get(0));
                //}

                    text = days[0] + " is associated with " + listeWorkouts2.get(0);
                    toast = Toast.makeText(this, text, duration);
                    toast.show();

                break;

            case R.id.buttonDeleteDays:
                daysDatasource.deleteAllDays();

                toast = Toast.makeText(this, "DAYS ASSOCIATION CLEANED", duration);
                toast.show();

                break;


            case R.id.buttonDeleteAllAssociation:

                workoutExerciceDatasource.deleteAllWorkoutExercices();

                text =  "ASSOCIATIONS DELETED";
                toast = Toast.makeText(this, text, duration);
                toast.show();
                break;
        }
    }
}
