package com.equipe03.gti350.fitnets;

import android.app.Activity;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.sql.SQLException;
import java.util.List;


public class OngoingActivity extends ActionBarActivity {

    private WorkoutsDataSource datasource;
    private WorkoutExercicesDataSource workoutExerciceDatasource;
    private int counter = 0;
    private int counterNext = 1;
    private int nbExercices = 0;
    private String currentexercice=null;
    private String nextexercice=null;
    private String currentExerciceRep=null;
    private List<Exercice> exercices=null;
    TextView tv;
    TextView tv2;
    TextView tv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoing);
        setTitle(R.string.title_activity_main);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();


        datasource = new WorkoutsDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutExerciceDatasource = new WorkoutExercicesDataSource(this);
        try {
            workoutExerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tv = (TextView)findViewById(R.id.ongoingactivity);
        tv2 = (TextView)findViewById(R.id.textCurrentExercice);
        tv3 = (TextView)findViewById(R.id.textBreak);
        Chronometer chrono = (Chronometer) findViewById(R.id.chronometer);
        chrono.setBase(SystemClock.elapsedRealtime());
        chrono.start();

        String workoutname = (String)bundle.get("workout");
        Workout currentWorkout=null;
        List<Workout> workouts = datasource.getAllWorkouts();

        for (int i = 0; i< workouts.size(); i++) {
            if(workouts.get(i).getWorkout().equals(workoutname)){
                currentWorkout = workouts.get(i);
            }
        }

        exercices= workoutExerciceDatasource.getAllWorkoutExercices(currentWorkout);

        nbExercices = exercices.size()-1;
        currentexercice = exercices.get(counter).getExercice();
        nextexercice = exercices.get(counterNext).getExercice();


        updateTvs();

        tv.setText("Ongoing Activity: " + currentWorkout.getWorkout());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ongoing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showTutorial(){

    }

    public void enterSet(){
        Intent intent = new Intent(this, EnterRepDoneActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        startActivityForResult(intent,1);
    }

    public void nextExercice(){
        if(counter<nbExercices) {
            counter++;
            currentexercice = exercices.get(counter).getExercice();
        }

        if(counterNext<nbExercices) {
            counterNext++;
            nextexercice = exercices.get(counterNext).getExercice();
        }else {
            nextexercice = null;
        }

        updateTvs();

    }

    public void stopActivity(){
        this.finish();
    }

    public void updateTvs(){
        if(currentexercice!=null ){
            tv2.setText(currentexercice );
        }else{
            tv2.setText("NO MORE EXERCICE");
        }

        if(nextexercice!=null){
            tv3.setText(nextexercice);
        }else{
            tv3.setText("NO MORE EXERCICE");
        }
    }


    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.buttonTutorial:
                showTutorial();
                break;
            case R.id.buttonDone:
                nextExercice();
                break;
            case R.id.buttonStopActivity:

                stopActivity();
                break;
            case R.id.buttonSetCompleted:
                enterSet();
                break;
            default:
                break;

        }
    }
}
