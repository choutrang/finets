package com.equipe03.gti350.fitnets;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ChooseDayActivity extends ActionBarActivity {
    private DaysDataSource daysDatasource;
    private List<Day> listdays;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_day);

        daysDatasource = new DaysDataSource(this);
        try {
            daysDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //List<Day> values = daysDatasource.getDays();
        List<String> values = new ArrayList<String>();

        String days[] = new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

        for (int i = 0; i<days.length; i++){
            values.add(days[i]);
        }

        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, values);


        //ArrayAdapter<Day> adapter = new DayListAdapter(this, values);


        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(adapter);

        listdays = daysDatasource.getDays();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Object dayClicked = lv.getAdapter().getItem(position);
                String text = "" + dayClicked.toString();
                String selectedDay = dayClicked.toString();

                /*
                for(int i = 0; i<listdays.size();i++) {


                    if(listdays.get(i).getDay().equals(selectedDay)){

                    }

                }


                Toast.makeText(getApplicationContext(), text,
                        Toast.LENGTH_SHORT).show();*/

                showWorkout(selectedDay);
            }

        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_day, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showWorkout(String day){
        Intent intent = new Intent(this, ChooseDayWorkoutActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra("selectedDay", day);
        startActivity(intent);

    }
}
