package com.equipe03.gti350.fitnets;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chou Trang on 7/15/2015.
 */
public class WorkoutsDataSource {
    // Database fields
    private SQLiteDatabase database;
    private NewSQLiteHelper dbHelper;
    private String[] allColumns = { NewSQLiteHelper.COLUMN_ID,
            NewSQLiteHelper.COLUMN_WORKOUT};

    public WorkoutsDataSource(Context context) {
        dbHelper = new NewSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Workout createExercice(String exercice) {
        ContentValues values = new ContentValues();
        values.put(NewSQLiteHelper.COLUMN_WORKOUT, exercice);

        long insertId = database.insert(NewSQLiteHelper.TABLE_WORKOUTS, null,
                values);
        Cursor cursor = database.query(NewSQLiteHelper.TABLE_WORKOUTS,
                allColumns, NewSQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Workout newWorkout = cursorToWorkout(cursor);
        cursor.close();
        return newWorkout;
    }

    public void deleteWorkout(Workout workout) {
        long id = workout.getId();
        System.out.println("Workout deleted with id: " + id);
        database.delete(NewSQLiteHelper.TABLE_WORKOUTS, NewSQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public void deleteAllWorkouts(){
        List<Workout> workouts = getAllWorkouts();

        for (Workout workout : workouts){
            database.delete(NewSQLiteHelper.TABLE_WORKOUTS, NewSQLiteHelper.COLUMN_ID
                    + " = " + workout.getId(), null);
        }
    }

    public List<Workout> getAllWorkouts() {
        List<Workout> workouts = new ArrayList<Workout>();

        Cursor cursor = database.query(NewSQLiteHelper.TABLE_WORKOUTS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Workout workout = cursorToWorkout(cursor);
            workouts.add(workout);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return workouts;
    }

    private Workout cursorToWorkout(Cursor cursor) {
        Workout workout = new Workout();
        workout.setId(cursor.getLong(0));
        workout.setComment(cursor.getString(1));
        return workout;
    }
}
