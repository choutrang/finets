package com.equipe03.gti350.fitnets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Chou Trang on 7/20/2015.
 * http://www.androidinterview.com/android-custom-listview-with-image-and-text-using-arrayadapter/
 * http://hmkcode.com/android-custom-listview-items-row/CustomListAdapter
 */
public class CustomListAdapter extends ArrayAdapter<Workout> {
    private final Context context;
    private final List<Workout> itemsArrayList;

    public CustomListAdapter(Context context, List<Workout> itemsArrayList) {

        super(context, R.layout.mylist, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.mylist, parent, false);

        // 3. Get the two text view from the rowView
        TextView labelView = (TextView) rowView.findViewById(R.id.Itemname);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        // 4. Set the text for textView

        String title = itemsArrayList.get(position).toString();
        labelView.setText(title);





        switch(title){
            case "Cardio Workout":
                imageView.setImageResource(R.drawable.cardio_workout);
                break;
            case "Strength Workout":
                imageView.setImageResource(R.drawable.strength_workout);
                break;
            case "Hypertrophy Workout":
                imageView.setImageResource(R.drawable.hypertrophy_workout);
                break;
            case "HIIT Workout":
                imageView.setImageResource(R.drawable.hiit_workout);
                break;
            case "Core Workout":
                imageView.setImageResource(R.drawable.core_workout);
                break;
            default:
                break;
        }

        return rowView;

    };
}
