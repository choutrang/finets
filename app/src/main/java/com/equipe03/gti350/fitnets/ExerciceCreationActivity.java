package com.equipe03.gti350.fitnets;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;


public class ExerciceCreationActivity extends ActionBarActivity {
    private ExercicesDataSource exerciceDatasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice_creation);

        exerciceDatasource = new ExercicesDataSource(this);
        try {
            exerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercice_creation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.buttonCreateExercice:
                createExercice();
                break;

            default:
                break;

        }
    }

    public void createExercice(){
        String exerciceName=null;
        String exercicesDesc=null;
        String defaultRep = "10,15,20";
        String defaultRest = "60";
        String message = "";

        EditText edExName = (EditText)findViewById(R.id.fexercicename);
        EditText edExDesc = (EditText)findViewById(R.id.fdescription);

        exerciceName=edExName.getText().toString();
        exercicesDesc=edExDesc.getText().toString();

        if(!exerciceName.matches("") && !exercicesDesc.matches("") && exerciceName.trim().length()>0
                && exercicesDesc.trim().length()>0) {

            exerciceDatasource.createExercice(exerciceName, exercicesDesc, defaultRep, defaultRest);
            message = "Exercice: " + exerciceName + " added!";
            finish();
        }else{
            message = "Please verify the information entered";
        }

        Toast.makeText(getApplicationContext(), message,
                Toast.LENGTH_SHORT).show();
    }
}
