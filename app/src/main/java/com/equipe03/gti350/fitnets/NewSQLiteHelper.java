package com.equipe03.gti350.fitnets;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Teaholik on 7/21/2015.
 * http://www.androidhive.info/2013/09/android-sqlite-database-with-multiple-tables/
 */
public class NewSQLiteHelper extends SQLiteOpenHelper {

    //GENERAL
    private static final String DATABASE_NAME = "workoutManager.db";
    private static final int DATABASE_VERSION = 10;

    //COMMON
    public static final String COLUMN_ID = "_id";

    //WORKOUT

    public static final String TABLE_WORKOUTS = "workouts";
    public static final String COLUMN_WORKOUT = "workout";

    //EXERCICE
    public static final String TABLE_EXERCICES = "exercices";
    public static final String COLUMN_EXERCICE = "exercice";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_REP = "rep";
    public static final String COLUMN_REST_TIME = "resttime";

    //workout_exercice
    public static final String TABLE_WORKOUT_EXERCICES = "workout_exercices";
    public static final String KEY_WORKOUT = "workout_id";
    public static final String KEY_EXERCICE = "exercice_id";

    //day
    public static final String TABLE_DAYS = "days";
    public static final String COLUMN_DAY = "day";

    // Database creation sql statement WORKOUT
    private static final String DATABASE_CREATE_WORKOUT = "create table "
            + TABLE_WORKOUTS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_WORKOUT
            + " text not null);";

    // Database creation sql statement EXERCICE
    private static final String DATABASE_CREATE_EXERCICE = "create table "
            + TABLE_EXERCICES + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_EXERCICE
            + " text not null, " + COLUMN_DESCRIPTION + " text not null, "
            + COLUMN_REP + " text not null, " + COLUMN_REST_TIME + " text not null);";

    // Database creation sql statement WORKOUT_EXERCICES
    private static final String DATABASE_CREATE_WORKOUT_EXERCICE = "CREATE TABLE "
            + TABLE_WORKOUT_EXERCICES + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + KEY_WORKOUT + " INTEGER, " + KEY_EXERCICE + " INTEGER, " + COLUMN_REP
            + " text not null, " + COLUMN_REST_TIME + " INTEGER);";

    // Database creation sql statement DAY
    private static final String DATABASE_CREATE_DAY = "create table "
            + TABLE_DAYS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_DAY
            + " text not null, " + COLUMN_WORKOUT + ");";


    public NewSQLiteHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //context.deleteDatabase("workoutManager.db");

    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DATABASE_CREATE_WORKOUT);
        database.execSQL(DATABASE_CREATE_EXERCICE);
        database.execSQL(DATABASE_CREATE_WORKOUT_EXERCICE);
        database.execSQL(DATABASE_CREATE_DAY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORKOUTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCICES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORKOUT_EXERCICES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DAYS);

        onCreate(db);
    }
}
