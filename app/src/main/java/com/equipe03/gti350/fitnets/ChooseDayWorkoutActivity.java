package com.equipe03.gti350.fitnets;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ChooseDayWorkoutActivity extends ListActivity {

    private WorkoutsDataSource datasource;
    private WorkoutExercicesDataSource workoutExerciceDatasource;
    private DaysDataSource daysDataSource;
    private Intent intent;
    private String selectedDay;
    private List<Day> listdays;
    private List<String> listWorkoutNameFound;
    private List<Workout> workoutList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_day_workout);

        intent = getIntent();
        Bundle bundle = intent.getExtras();
        selectedDay = bundle.getString("selectedDay");

        loadDatabase();

        listdays = daysDataSource.getDays();

        List<Workout> listWorkout = datasource.getAllWorkouts();

        String message = "";

        listWorkoutNameFound = new ArrayList<String>();
        for (int i = 0; i<listdays.size(); i++) {
            if(listdays.get(i).getDay().toString().equals(selectedDay)) {
                listWorkoutNameFound.add(listdays.get(i).getWorkout());
            }
        }

        workoutList = new ArrayList<Workout>();
        for (Workout workout: listWorkout){
            for(int z = 0; z < listWorkoutNameFound.size(); z++) {
                if(workout.getWorkout().equals(listWorkoutNameFound.get(z))){
                    workoutList.add(workout);
                }
            }
        }


        //List<Workout> values = datasource.getAllWorkouts();

        ArrayAdapter<Workout> adapter = new CustomListAdapter(this, workoutList);

        setListAdapter(adapter);
    }


    // http://stackoverflow.com/questions/10299426/how-to-implement-onitemclicklistener-in-list-activity
    public void onListItemClick(ListView listView, View view, int position, long id) {
        ListView myListView = getListView();
        Object workoutClicked = myListView.getAdapter().getItem(position);
        startWorkout(workoutClicked);
    }


    @Override
    protected void onResume() {
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

    public void startWorkout(Object workout){
        Intent intent = new Intent(this, OngoingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String workoutName = ((Workout) workout).getWorkout();

        List<Exercice> exercices = workoutExerciceDatasource.getAllWorkoutExercices((Workout)workout);

        intent.putExtra("workout" ,workoutName);
        finish();
        startActivity(intent);
    }

    public void loadDatabase(){
        datasource = new WorkoutsDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutExerciceDatasource = new WorkoutExercicesDataSource(this);
        try {
            workoutExerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        daysDataSource = new DaysDataSource(this);
        try {
            daysDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
