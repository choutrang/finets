package com.equipe03.gti350.fitnets;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.List;


public class ChooseWorkoutActivity extends ListActivity {

    private WorkoutsDataSource datasource;
    private WorkoutExercicesDataSource workoutExerciceDatasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_workout);

        datasource = new WorkoutsDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        workoutExerciceDatasource = new WorkoutExercicesDataSource(this);
        try {
            workoutExerciceDatasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Workout> values = datasource.getAllWorkouts();

        ArrayAdapter<Workout> adapter = new CustomListAdapter(this, values);

        setListAdapter(adapter);


    }

    // http://stackoverflow.com/questions/10299426/how-to-implement-onitemclicklistener-in-list-activity
    public void onListItemClick(ListView listView, View view, int position, long id) {
        ListView myListView = getListView();
        Object workoutClicked = myListView.getAdapter().getItem(position);
        startWorkout(workoutClicked);
        Toast.makeText(getApplicationContext(), "Workout Clicked: " + workoutClicked + " \nPosition: " + id,
                Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onResume() {
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

    public void startWorkout(Object workout){
        Intent intent = new Intent(this, OngoingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String workoutName = ((Workout) workout).getWorkout();

        List<Exercice> exercices = workoutExerciceDatasource.getAllWorkoutExercices((Workout)workout);

        intent.putExtra("workout" ,workoutName);
        finish();
        startActivity(intent);
    }
}
