package com.equipe03.gti350.fitnets;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chou Trang on 7/15/2015.
 */
public class WorkoutExercicesDataSource {
    // Database fields
    private SQLiteDatabase database;
    private NewSQLiteHelper dbHelper;
    private String[] allColumns = { NewSQLiteHelper.COLUMN_ID,
            NewSQLiteHelper.KEY_WORKOUT, NewSQLiteHelper.KEY_EXERCICE };

    public WorkoutExercicesDataSource(Context context) {
        dbHelper = new NewSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long createWorkoutExercice(Workout workout, Exercice exercice, String rep, int rest) {
        ContentValues values = new ContentValues();
        values.put(NewSQLiteHelper.KEY_WORKOUT, workout.getId());
        values.put(NewSQLiteHelper.KEY_EXERCICE, exercice.getId());
        values.put(NewSQLiteHelper.COLUMN_REP, rep);
        values.put(NewSQLiteHelper.COLUMN_REST_TIME, rest);

        long insertId = database.insert(NewSQLiteHelper.TABLE_WORKOUT_EXERCICES, null,
                values);

        return insertId;
    }

    public void deleteWorkoutExercice(Workout workout, Exercice exercice) {
        ContentValues values = new ContentValues();
        values.put(NewSQLiteHelper.KEY_EXERCICE, exercice.getId());

        database.update(NewSQLiteHelper.TABLE_WORKOUT_EXERCICES, values, NewSQLiteHelper.COLUMN_ID + " = ?",
                new String[] { String.valueOf(workout.getId()) });
    }

    public void deleteAllWorkoutExercices(){
            database.delete(NewSQLiteHelper.TABLE_WORKOUT_EXERCICES, null,null);
    }

    //get all exercice under a single workout
    public List<Exercice> getAllWorkoutExercices(Workout workout){
        List<Exercice> exercices = new ArrayList<Exercice>();

        String selectQuery = "SELECT  * FROM " + NewSQLiteHelper.TABLE_EXERCICES + " ex, "
                + NewSQLiteHelper.TABLE_WORKOUTS + " wk, " + NewSQLiteHelper.TABLE_WORKOUT_EXERCICES + " wkex WHERE wk."
                + NewSQLiteHelper.COLUMN_WORKOUT + " = '" + workout + "'" + " AND wk." + NewSQLiteHelper.COLUMN_ID
                + " = " + "wkex." + NewSQLiteHelper.KEY_WORKOUT + " AND ex." + NewSQLiteHelper.COLUMN_ID + " = "
                + "wkex." + NewSQLiteHelper.KEY_EXERCICE;



        Cursor c = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Exercice td = new Exercice();
                td.setId(c.getInt((c.getColumnIndex(NewSQLiteHelper.COLUMN_ID))));
                td.setExercice((c.getString(c.getColumnIndex(NewSQLiteHelper.COLUMN_EXERCICE))));
                td.setDescription((c.getString(c.getColumnIndex(NewSQLiteHelper.COLUMN_DESCRIPTION))));
                td.setRep((c.getString(c.getColumnIndex(NewSQLiteHelper.COLUMN_REP))));
                td.setRestTime((c.getString(c.getColumnIndex(NewSQLiteHelper.COLUMN_REST_TIME))));

                // adding to todo list
                exercices.add(td);
            } while (c.moveToNext());
        }

        return exercices;
    }

}
