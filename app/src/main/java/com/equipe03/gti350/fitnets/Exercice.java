package com.equipe03.gti350.fitnets;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Chou Trang on 7/15/2015.
 */
public class Exercice{
    private long id;
    private String exercice;
    private String description;
    private String rep;
    private String restTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRep(String rep) {
        this.rep = rep;
    }

    public void setRestTime(String rest) {
        this.restTime = rest;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return exercice;
    }


}
