package com.equipe03.gti350.fitnets;

/**
 * Created by Chou Trang on 7/15/2015.
 */
public class Day {
    private long id;
    private String day;
    private String workout;
    private String rep;
    private String restTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public String getWorkout() {
        return workout;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setWorkout(String workout) {
        this.workout = workout;
    }

    public void setRep(String rep) {
        this.rep = rep;
    }

    public void setRestTime(String rest) {
        this.restTime = rest;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return day;
    }
}
